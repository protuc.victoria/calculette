package calcfx;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.paint.Color;
import javafx.scene.paint.Paint;
import javafx.stage.Stage;

import java.nio.file.Paths;


public class Main extends Application {

  public static void main(String[] args) {

  Application.launch(args);
  }

  @Override
  public void start(Stage stage) throws Exception {
    try{
      Parent root = FXMLLoader.load(Paths.get("src/calcfx/Main.fxml").toUri().toURL());
      Scene scene = new Scene(root,300,340);
      stage.setTitle("sexy calculatrice ");

      stage.setScene(scene);

      stage.show();
    }catch (Exception e){
      e.printStackTrace();
    }
  }
}
