package calcfx;

import calcfx.Calculette.Calculette;
import calcfx.Calculette.Parser;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;

public class MainController {

  @FXML
  private Label result;
  private String tmp = "0";



  @FXML
  public void processNumbers(ActionEvent event){
    String value = ((Button)event.getSource()).getText();

    if (result.getText().equals("0"))
      result.setText(value);
    else
    result.setText(result.getText() + value);
  }
  @FXML
  public void  processClear (ActionEvent event){
    if(((Button)event.getSource()).getText().equals("C")) {
      result.setText(tmp);
    } else
    {
      result.setText("0");
      tmp = "0";

    }

  }

  @FXML
  public void processEqual (ActionEvent event){
    String value = ((Button)event.getSource()).getText();
    System.out.printf(result.getText());
    result.setText( String.valueOf((Parser.parse_on(result.getText()).eval())));
    tmp = result.getText() ;
  }

}
//