package calcfx.Calculette.Expressions.Unary_Opps;

import calcfx.Calculette.Expressions.Expr_Unary;
import calcfx.Calculette.Expressions.Expression;

public class NEUTRAL extends Expr_Unary {
  public NEUTRAL(Expression left) {
    super(left);
  }

  @Override
  public int eval() {
    return this.left.eval();
  }
}
