package calcfx.Calculette.Expressions.Unary_Opps;

import calcfx.Calculette.Expressions.Expr_Unary;
import calcfx.Calculette.Expressions.Expression;

public class NEG extends Expr_Unary {

  public NEG(Expression left) {
    super(left);
  }

  @Override
  public int eval() {
    return this.left.eval() * (-1);
  }
}
