package calcfx.Calculette.Expressions.Bin_Opps;

import calcfx.Calculette.Expressions.Expr_Binary;
import calcfx.Calculette.Expressions.Expression;

public class DIV extends Expr_Binary {
  public DIV(Expression left, Expression right) {
    super(left, right);
  }

  @Override
  public int eval(){
    int value = 0;
     try {
       value = this.left.eval() / this.right.eval();  }
     catch(ArithmeticException e) { }
    return value;
  }
}
