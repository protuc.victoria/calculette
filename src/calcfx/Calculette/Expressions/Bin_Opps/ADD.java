package calcfx.Calculette.Expressions.Bin_Opps;

import calcfx.Calculette.Expressions.Expr_Binary;
import calcfx.Calculette.Expressions.Expression;

public class ADD extends Expr_Binary {

  public ADD(Expression left, Expression right) {
    super(left, right);
  }

  @Override
  public int eval(){
    return this.left.eval() + this.right.eval();
  }
}
