package calcfx.Calculette.Expressions.Bin_Opps;

import calcfx.Calculette.Expressions.Expr_Binary;
import calcfx.Calculette.Expressions.Expression;

public class SUB  extends Expr_Binary {
  public SUB(Expression left, Expression right) {
    super(left, right);
  }

  @Override
  public int eval(){
    return this.left.eval() - this.right.eval();
  }
}
