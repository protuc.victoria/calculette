package calcfx.Calculette.Expressions;

public abstract class Expr_Unary implements Expression {
  protected Expression left;

  public Expr_Unary(Expression left) {
    this.left = left;
  }
}
