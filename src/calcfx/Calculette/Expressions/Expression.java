package calcfx.Calculette.Expressions;


public interface Expression {
  int eval();
}
