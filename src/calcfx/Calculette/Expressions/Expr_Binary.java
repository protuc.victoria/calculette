package calcfx.Calculette.Expressions;

public abstract class Expr_Binary implements Expression{

  protected Expression left;
  protected Expression right;


  public Expr_Binary(Expression left, Expression right) {
    this.left = left;
    this.right = right;
  }
}

