package calcfx.Calculette;

import calcfx.Calculette.Expressions.Expression;

public class CST implements Expression {
  int value;

  public CST(int value) {
    this.value = value;
  }


  @Override
  public int eval() {
    return this.value;
  }
}
