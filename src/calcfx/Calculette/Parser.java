package calcfx.Calculette;

import calcfx.Calculette.Expressions.Bin_Opps.ADD;
import calcfx.Calculette.Expressions.Bin_Opps.DIV;
import calcfx.Calculette.Expressions.Bin_Opps.MUL;
import calcfx.Calculette.Expressions.Bin_Opps.SUB;
import calcfx.Calculette.Expressions.Expression;
import calcfx.Calculette.Expressions.Unary_Opps.NEG;
import calcfx.Calculette.Expressions.Unary_Opps.NEUTRAL;

public class Parser {
  private static String src;
  private static int index;

  private static char last_char;
  private static int last_cst;

  private static boolean read_char(char c){
    if( (Parser.index < Parser.src.length()) && (Parser.src.charAt(Parser.index)==c) ){
      Parser.index++;
      Parser.last_char=c;
      return true;
    }
    return false;
  }

  private static boolean read_cst() {

    boolean  flagcst=false;
    boolean flag = false;
    StringBuffer buffer=new StringBuffer(64);

    do {
      flag=false;
      for (int i = 0; i < 10; i++) {
        if(read_char((char)(i+'0'))){
          buffer.append(last_char);
          flag=true;
          flagcst=true;
        }
      }

    }while(flag);
    last_cst= Integer.parseInt(buffer.toString());

    return flagcst;
  }


private static Expression read_e() {
    Expression result,right;
    char op;
    result=read_e_mul();
    if(result!=null){
      while ((read_char('+')||read_char('-'))) {
        op=last_char;
        right=read_e_mul();
        if (right==null) error();
        if(op=='+')
          result= new ADD(result,right);
        else
          result=new SUB(result,right);
      }

  }

return result;
}

private static Expression read_e_mul() {
  Expression result, right;
  char op;
  result = read_e_unary();
  if (result != null) {
    while ((read_char('*') || read_char('/'))) {
      op = last_char;
      right = read_e_unary();
      if (right == null) error();
      if (op == '*') {
        result = new MUL(result, right);
      }
      else {
        result = new DIV(result, right);
      }
    }
  }
  return result;
}


  private static Expression read_e_unary(){
    Expression result;


    if(read_char('-')) {
      result=read_e_cst();
      return new NEG(result);
    }
    else {
      result=read_e_cst();
      return new NEUTRAL(result);
    }


  }

  private static Expression read_e_cst(){
    Expression expression = null;
    if (read_char('(')){
      expression = read_e();
      if(!read_char(')'))
        error();
    }else {
      if (read_cst())
        expression = new CST(last_cst);
    }

    return expression;
  }


  private static void error() {


    int j;
    System.out.println(src);
    for(j=0;j<index;j++)
      System.out.println(' ');
    System.out.println('I');
    System.exit(1);
  }





  public static Expression  parse_on(String txt){
    Expression e;
    src=txt;
    index=0;
    e=read_e();
    if((e==null)||(index<src.length()))
      error();
    return e;
  }




}
