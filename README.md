# Rapport

**Calculatrice Version avec des opérands entiers**

> Ici on utilise le TP3 (la classe Parser ...) comme "moteur"  pour la Calculatrice graphique.

Le Tp3 Calculatrice version console se trouve dans le package **calcfx/Calculette**.

Pour implémenter graphiquement la calculatrice j'ai choisi la bibliothèque Java FX car  meme si cette GUI est plus jeune 
que Swing et AWT ,il y a assez des tutos  sur Java FX sur Internet et  elle facilite l'implémentation graphique.
    
  La classe ***Main***  a l'aide de FXMLLoader charge le fichier de  la mise en scene (Main.fxml).Notre classe ***Main***
 hérite de la classe ***Application***. La classe ***MainController*** contient les méthodes qui traitent les événements 
 qui sont associés aux boutons de la calculatrice.

En utilisant la bibliothèque graphique **java FX**  avec l'aide du logiciel  Scene Builder, on crée les boutons et le
**Label** nécessaire pour afficher le résultat.

 A chaque bouton on associe une méthode qui sera déclenchée lorsque un ActionEvent se produit,dans le cas des boutons
 l'ActionEvent est le click sur le bouton.
 

 ## Algorithme 


  En Clickant  sur les Boutons on obtient  l' expression, cette  expression arithmétique (ex. : 2+3*(4-6) etc) est prise 
  en charge par le **Parser**, lui il nous donne  le résultat  qui sera affiché dans le **Label** resultat.
   
 Lorsque le bouton **=** est  appuyé, un résultat temporaire  ***private String tmp*** sera utilisé pour le 
 fonctionnememnt du bouton **C**.
  
 Le click sur le bouton **AC**  met le label,ainsi que le résultat temporaire (le tmp )  à **0** (vide ) 